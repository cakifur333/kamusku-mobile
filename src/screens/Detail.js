import React, { Component } from 'react';
import { View, StatusBar, Text, TextInput, ToastAndroid, FlatList, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Clipboard  } from 'react-native';


class Detail extends Component{

  constructor(props) {
    super(props);
  }
// clipboard
  salin = (value)=>{
    Clipboard.setString(value);
    ToastAndroid.show(value, ToastAndroid.SHORT);
  }
  render() {
    //yg d bwh
    const { indonesia } = this.props.route.params;
    const { english } = this.props.route.params;


    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#2962ff" barStyle="light-content" />

        <View style={{padding: 20, backgroundColor: '#2196f3', elevation: 1, flexDirection: 'row', alignItems: 'center'  }}>
          <Icon name="arrow-left" size={25} color="#FFFFFF" style={{marginRight: 10}} onPress={()=> this.props.navigation.pop()} /> 
          <Text style={{textAlign:'center', color:'#FFFFFF', fontWeight: 'bold', fontSize: 18}}>Detail</Text>
        </View>

        <Text style={{ textAlign: 'center', marginTop:20, fontWeight: 'bold', fontSize: 30, color:'#2196f3'}}>{indonesia}</Text>
        <Text style={{ textAlign: 'center', marginTop:5, fontSize: 15, color:'black'}}>{english}</Text>

        <TouchableOpacity onPress={() => this.salin(english)} style={{justifyContent: 'center', padding:10,alignItems:'center', backgroundColor: 'green', margin:10, borderRadius: 6}}>
          <Text style={{color:'FFFFFF', textAlign:'center'}}>Salin</Text>
        </TouchableOpacity>


      </View>
    );
  }
}


export default Detail;
