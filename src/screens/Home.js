import React, { Component } from 'react';
import { View, StatusBar, Text, TextInput, FlatList, TouchableOpacity, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
import { Logo } from '../img';

let database = [
  {indonesia: 'Ayam', english: 'Chicken'},
  {indonesia: 'Anjing', english: 'Dog'},
  {indonesia: 'Kucing', english: 'Cat'},
  {indonesia: 'Kelinci', english: 'Rabbit'},
  {indonesia: 'Ikan', english: 'Fish'},
  {indonesia: 'Sapi', english: 'Cow'},
  {indonesia: 'Anak Sapi', english: 'Calves'},
  {indonesia: 'Kambing', english: 'Goat'},
  {indonesia: 'Kuda', english: 'Horse'},
  {indonesia: 'Jerapah', english: 'Giraffe'},
  {indonesia: 'Rusa', english: 'Deer'},
  {indonesia: 'Panda Raksasa', english: 'Giant Panda'},
  {indonesia: 'Badak', english: 'Rhinoceros'},
  {indonesia: 'Keledai', english: 'Donkey'},
  {indonesia: 'Zebra', english: 'Zebra'},
  {indonesia: 'Camel', english: 'Unta'},
  {indonesia: 'Angsa', english: 'Swan'},
  {indonesia: 'Marmut', english: ' Guinea Pig'},


]

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      text: '',
      data: database
     };
  }
  

  search = () => {
    let data = database;

    data = data.filter(item => item.indonesia.toLocaleLowerCase().includes(this.state.text.toLocaleLowerCase()));

    this.setState({
      data:data
    })
  }

  render() {
    return (
      <View style={{flex: 1}}>
        
        <StatusBar backgroundColor="#2962ff" barStyle="light-content" />

        <View style={{padding: 20, backgroundColor: '#2196f3', elevation: 1 , display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
          <Image source={Logo} style={{width: 50, height: 50}} />
          <Text style={{textAlign:'center', color:'#000000', fontWeight: 'bold', fontSize: 18}}>KAMUSKU</Text>
        </View>

        <TextInput
        style={{margin: 10,height: 40, color: '#86E5FF', borderColor: 'gray', borderWidth: 1, paddingleft: 10, marginVertial: 20, marginHorizontal:10, borderRadius:5 }}
        onChangeText={text => this.setState({text :text})}
        value={this.state.text}
        placeholder='Masukkan kata kunci'
        placeholderTextColor={434242}
        onKeyPress={() => this.search()}
      />
      
      <FlatList
        data={this.state.data}
        renderItem={({item}) => (
          <TouchableOpacity style={{margin:2, borderWidth: 1, borderRadius: 3, marginVertial: 5, marginHorizontal: 20, padding: 10 }}
            onPress={() => this.props.navigation.navigate('Detail', {indonesia: item.indonesia, english: item.english}) }
            //diatas
          >
            <Text style={{fontSize: 18, color: 'black', fontWeight: 'bold'}}>{item.indonesia}</Text>
            <Text style={{fontSize: 16, color: 'black', marginTop: 5}}>{item.english}</Text>
          </TouchableOpacity>
        )
        }
        keyExtractor={item => item.indonesia}
      />


      </View>
    );
  }
}

export default Home;
